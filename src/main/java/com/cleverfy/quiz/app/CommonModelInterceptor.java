package com.cleverfy.quiz.app;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.cleverfy.quiz.mapper.UserMapper;
import com.cleverfy.quiz.util.SecurityUtil;

@Component
public class CommonModelInterceptor extends HandlerInterceptorAdapter {
	
	@Autowired
	UserMapper userMapper;
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
	        ModelAndView modelAndView) throws Exception {
		
		if (modelAndView != null && modelAndView.getViewName() != null && !modelAndView.getViewName().startsWith("redirect:")) {
			
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			if (authentication != null && !(authentication instanceof AnonymousAuthenticationToken)) {				
				modelAndView.getModelMap().addAttribute("user", userMapper.findOne(SecurityUtil.getCurrentUserId()));
				modelAndView.getModelMap().addAttribute("signedIn", true);
			} else {
				modelAndView.getModelMap().addAttribute("signedIn", false);
			}
			modelAndView.getModelMap().addAttribute("_csrf", request.getAttribute("_csrf"));						
		}
		super.postHandle(request, response, handler, modelAndView);
	}
}
