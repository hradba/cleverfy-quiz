package com.cleverfy.quiz.app;

import java.beans.PropertyVetoException;

import javax.annotation.Resource;
import javax.sql.DataSource;

import liquibase.integration.spring.SpringLiquibase;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages={"com.cleverfy.quiz.service"})
@MapperScan("com.cleverfy.quiz.mapper")
public class PersistenceConfig {

    @Resource
    private Environment environment;

	@Bean
	public SpringLiquibase liquibase() throws IllegalStateException, PropertyVetoException {
		SpringLiquibase springLiquibase = new SpringLiquibase();
		springLiquibase.setDataSource(dataSource());
		springLiquibase.setChangeLog("classpath:database-changelog.xml");			
		return springLiquibase;
	}
	
    @Bean
    public DataSource dataSource() throws IllegalStateException, PropertyVetoException {
    	ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setDriverClass(environment.getRequiredProperty("db.driver"));
        dataSource.setJdbcUrl(environment.getRequiredProperty("db.url"));
        dataSource.setUser(environment.getRequiredProperty("db.user"));
        dataSource.setPassword(environment.getRequiredProperty("db.password"));
    	return dataSource;
    }
    
    @Bean
    public DataSourceTransactionManager transactionManager() throws IllegalStateException, PropertyVetoException {
    	return new DataSourceTransactionManager(dataSource());
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
      SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
      sessionFactory.setDataSource(dataSource());       
      return sessionFactory.getObject();
    }
}
