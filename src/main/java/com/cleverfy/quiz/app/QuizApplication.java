
package com.cleverfy.quiz.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class QuizApplication extends SpringBootServletInitializer {

	final static Logger logger = LoggerFactory.getLogger(QuizApplication.class);
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(QuizApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(QuizApplication.class, args);
	}
	
}
