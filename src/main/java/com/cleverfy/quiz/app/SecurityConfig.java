package com.cleverfy.quiz.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.cleverfy.quiz.mapper.UserMapper;
import com.cleverfy.quiz.model.User;

@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	UserMapper userMapper;
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/", "/assets/**", "/sign-up", "/activate/*").permitAll()
                .anyRequest().authenticated()
                .and()
            .formLogin()            
                .loginPage("/log-in")
                .usernameParameter("email")
                .passwordParameter("password")
                .permitAll()                
                .defaultSuccessUrl("/quiz-list")
                .and()
            .logout()   
            	.logoutUrl("/log-out")
            	.logoutSuccessUrl("/")
                .permitAll();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    	auth.authenticationProvider(new AuthenticationProvider() {
			
			@Override
			public boolean supports(Class<?> authentication) {
				return authentication.equals(UsernamePasswordAuthenticationToken.class);
			}
			
			@Override
			public Authentication authenticate(Authentication authentication) throws AuthenticationException {
				String email = authentication.getName();
		        String password = authentication.getCredentials().toString();
		        
		        User user = userMapper.findByEmail(email);
		        if (user != null && user.passwordMatches(password)) {
		        	if (!user.isActivated()) {		        		
		        		return null;
		        	}
		            List<SimpleGrantedAuthority> grantedAuths = new ArrayList<>(Arrays.asList(new SimpleGrantedAuthority("USER")));
		            Authentication auth = new UsernamePasswordAuthenticationToken(user.getId(), password, grantedAuths);		            
		            return auth;
		        } else {
		            return null;
		        }
			}
		});
    }
    

}