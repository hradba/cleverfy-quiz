package com.cleverfy.quiz.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class ExceptionHandlerControllerAdvice {
	
	// TODO 404 handling... (dispatcher servlet needs to be configured)
	@ExceptionHandler(NoHandlerFoundException.class)
	public ModelAndView handleError404(HttpServletRequest request, Exception e) {
		ModelAndView mav = new ModelAndView("/error-404");
		mav.addObject("exception", e);
		return mav;
	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
		// If the exception is annotated with @ResponseStatus rethrow it and let the framework handle it - like the
		// OrderNotFoundException example at the start of this post. AnnotationUtils is a Spring Framework utility
		// class.
		if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
			throw e;
		
		ModelAndView mav = new ModelAndView("/error-page");
		mav.addObject("exception", e);
		mav.addObject("url", req.getRequestURL());
		
		return mav;
	}
}
