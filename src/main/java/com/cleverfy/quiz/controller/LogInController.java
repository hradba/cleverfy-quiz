package com.cleverfy.quiz.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LogInController {
	
	@RequestMapping("/log-in")
	public String login(Model model, HttpServletRequest request) {
		if (request.getParameterMap().containsKey("error")) {
			model.addAttribute("loginFailed", true);
		}
		return "log-in";
	}
}
