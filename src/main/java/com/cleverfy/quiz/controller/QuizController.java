package com.cleverfy.quiz.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cleverfy.quiz.model.Pagination;
import com.cleverfy.quiz.model.Question;
import com.cleverfy.quiz.model.Quiz;
import com.cleverfy.quiz.service.QuizExecutorService;
import com.cleverfy.quiz.service.QuizService;
import com.cleverfy.quiz.util.SecurityUtil;

@Controller
class QuizController {

	@Autowired
	private QuizService quizService;
	
	@Autowired
	private QuizExecutorService quizExecutorService;

	public String getTitle() {
		return "Quiz";
	}

	@RequestMapping(value = "/quiz-add-extra-questions/{id}", method = RequestMethod.POST)
	public String quizAddExtraQuestions(@PathVariable Long id, Model model, RedirectAttributes redirectAttributes) {
		quizExecutorService.addExtraQuestions(id, SecurityUtil.getCurrentUserId());
		redirectAttributes.addFlashAttribute("extraQuestionAddedToQueue", true);
		return "redirect:/quiz-detail/" + id;
	}
	
	@RequestMapping(value = "/quiz-delete/{id}", method = RequestMethod.POST)
	public String quizDelete(@PathVariable Long id, Model model, RedirectAttributes redirectAttributes) {
		quizService.deleteQuiz(id, SecurityUtil.getCurrentUserId());
		redirectAttributes.addFlashAttribute("quizDeleted", true);
		return "redirect:/quiz-list/";
	}
	
	@RequestMapping("/quiz-detail/{id}")
	public String quizDetail(@PathVariable Long id, Model model, RedirectAttributes redirectAttributes) {
		Quiz quiz = quizService.getUserQuiz(id, SecurityUtil.getCurrentUserId());
		long questionForReviewCount = quizExecutorService.getQuestionForReviewCount(quiz);
		model.addAttribute("questionForReviewCount", questionForReviewCount);
		model.addAttribute("hasQuestionsForReview", questionForReviewCount > 0 ? true : false);
		model.addAttribute("quiz", quiz);
		model.addAttribute("name", getTitle());
		model.addAttribute("quizListActive", true);		
		model.addAttribute("generationNotCompleted", !quiz.isGenerationCompleted());
		
		return "quiz-detail";
	}
	
	@RequestMapping("/quiz-browse/{id}")
	public String quizBrowse(@PathVariable Long id, Model model, RedirectAttributes redirectAttributes) {
		return quizBrowse(id, 0, 10, model, redirectAttributes);
	}
	
	@RequestMapping("/quiz-browse/{id}/offset/{offset}/limit/{limit}")
	public String quizBrowse(@PathVariable Long id, @PathVariable int offset, @PathVariable int limit, Model model, RedirectAttributes redirectAttributes) {
		Quiz quiz = quizService.getUserQuiz(id, SecurityUtil.getCurrentUserId());
		List<Question> quizQuestions = quizService.getQuizQuestions(quiz.getId(), offset, limit);
		model.addAttribute("quiz", quiz);
		model.addAttribute("name", getTitle());
		model.addAttribute("questions", quizQuestions);
		model.addAttribute("pagination", new Pagination("/quiz-browse/" + id, offset, limit, quiz.getTotalQuestionCount()));
		model.addAttribute("generationNotCompleted", !quiz.isGenerationCompleted());
		model.addAttribute("quizListActive", true);
		return "quiz-browse";
	}
	
	@RequestMapping("/quiz-execute/{quizId}") 
	public String quizStart(@PathVariable Long quizId, Model model, RedirectAttributes redirectAttributes) {
		quizExecutorService.getCurrentQuestion(quizId, new Date());
		model.addAttribute("title", getTitle());
		model.addAttribute("quizListActive", true);
		return "redirect:/quiz-execute/" + quizId + "/question";
	}
	
	@RequestMapping("/quiz-execute/{quizId}/question")
	public String quizQuestion(@PathVariable Long quizId, Model model, RedirectAttributes redirectAttributes) {
		Question currentQuestion = quizExecutorService.getCurrentQuestion(quizId, new Date());
		
		if (currentQuestion != null) {
			model.addAttribute("quiz", quizService.getUserQuiz(quizId, SecurityUtil.getCurrentUserId()));		
			model.addAttribute("question", currentQuestion);
			model.addAttribute("progress", quizExecutorService.getQuizProgress(quizId));		
			model.addAttribute("title", getTitle());
			model.addAttribute("quizListActive", true);
			return "quiz-execute";	
		} else {
			redirectAttributes.addFlashAttribute("quizFinished", true);
			return "redirect:/quiz-detail/" + quizId;
		}		
	}
	
	
	@RequestMapping(value = "/quiz-execute/{quizId}/answer/{questionId}", method = RequestMethod.POST)
	public String quizAnswer(@PathVariable Long quizId, @PathVariable Long questionId, @RequestParam(required=false) String answer, Model model, RedirectAttributes redirectAttributes) {
		if (answer == null) {
			redirectAttributes.addFlashAttribute("answerIsMissing", true);
			return "redirect:/quiz-execute/" + quizId + "/question";
		}
		Boolean correctAnswer = quizExecutorService.answer(quizId, questionId, SecurityUtil.getCurrentUserId(), answer);
		if (correctAnswer != null && correctAnswer) {
			redirectAttributes.addFlashAttribute("correctAnswer", true);			
		} else if (correctAnswer != null) {
			redirectAttributes.addFlashAttribute("wrongAnswer", true);
		}
		return "redirect:/quiz-execute/" + quizId + "/question";
	}
		
	@RequestMapping("/quiz-list")
	public String quizList(Model model) {
		model.addAttribute("quizzes", quizService.getUserQuizzes(SecurityUtil.getCurrentUserId()));
		model.addAttribute("title", "My Quizzes");
		model.addAttribute("quizListActive", true);
		return "quiz-list";
	}
}
