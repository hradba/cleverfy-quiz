package com.cleverfy.quiz.controller;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cleverfy.quiz.service.QuizService;
import com.cleverfy.quiz.util.SecurityUtil;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleExtractor;

@Controller
class QuizCreateController {

	@Autowired
	private QuizService quizService;

	public String getTitle() {
		return "Create Quiz";
	}

	@RequestMapping(value = "/extract-url-article*", method = RequestMethod.GET)
	@ResponseBody
	public UrlArticle extractUrlArticle(HttpServletRequest request) throws MalformedURLException, BoilerpipeProcessingException {
		String url = request.getParameter("url");
		String article = ArticleExtractor.getInstance().getText(new URL(url));
		return new UrlArticle(article);
	}
	
	@RequestMapping(value = "/quiz-create", method = RequestMethod.GET)
	public String generate(Model model) {		
		model.addAttribute("title", getTitle());
		model.addAttribute("quizCreateActive", true);
		return "quiz-create";
	}
	
	@RequestMapping(value = "/quiz-create", method = RequestMethod.POST)
	public String generate(@RequestParam("name") String name, @RequestParam("text") String text, RedirectAttributes redirectAttributes) {
		Long id = quizService.generateQuiz(SecurityUtil.getCurrentUserId() ,name, text);
		return "redirect:quiz-detail/" + id;
	}
	
	class UrlArticle {
		private String article;

		
		public UrlArticle(String article) {
	        super();
	        this.article = article;
        }

		public String getArticle() {
			return article;
		}

		
	}
}
