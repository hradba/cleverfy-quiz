package com.cleverfy.quiz.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cleverfy.quiz.service.UserService;

@Controller
public class SignUpController {
	
	final static Logger logger = LoggerFactory.getLogger(SignUpController.class);

	@Autowired
	UserService userService;
	
	@RequestMapping(value = "/activate/{activationHash}", method = RequestMethod.GET)
	public String activate(@PathVariable String activationHash, RedirectAttributes redirectAttributes) {
		userService.activateUser(activationHash);				
		redirectAttributes.addFlashAttribute("userActivated", true);		
		return "redirect:/log-in";
	}

	@RequestMapping(value = "/sign-up", method = RequestMethod.GET)
	public String signUp() {		
		return "sign-up";
	}
		
	@RequestMapping(value = "/sign-up", method = RequestMethod.POST)
	public String signUp(@RequestParam("email") String email, @RequestParam("password") String password, RedirectAttributes redirectAttributes) {
		userService.signUpUser(email, password);
		redirectAttributes.addFlashAttribute("verficationEmailSent", true);
		return "redirect:/log-in";
	}
}
