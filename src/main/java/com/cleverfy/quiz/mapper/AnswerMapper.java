package com.cleverfy.quiz.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;

import com.cleverfy.quiz.model.Answer;

public interface AnswerMapper {
	

	@SelectKey(keyProperty="id", resultType=Long.class, before=true, statement = { "SELECT NEXTVAL('cf_answers_seq') AS id" })
	@Insert("INSERT INTO CF_ANSWERS (id, answer, correct, question_id) VALUES (#{id}, #{answer}, #{correct}, #{questionId})")	
	public void save(Answer answer);
	
	@Select("SELECT * FROM CF_ANSWERS WHERE id = #{answerId}")
	@ResultMap("AnswerResultMap")
	public Answer findOne(Long answerId);	

	@Select("SELECT * FROM CF_ANSWERS WHERE question_id = #{questionId} AND correct = true")
	@ResultMap("AnswerResultMap")
	public Answer findQuestionAnswer(Long questionId);
	
	@Delete("DELETE FROM CF_ANSWERS a "
			+ "WHERE a.question_id IN ("
				+ "SELECT q.id FROM CF_QUESTIONS q "
				+ "LEFT JOIN CF_QUIZZES qi ON (qi.id = q.quiz_id) "
				+ "WHERE q.quiz_id = #{quizId} AND qi.owner_user_id = #{userId}"
			+ ")")
	public void deleteAnswers(@Param("quizId") Long quizId, @Param("userId") Long userId);

}
