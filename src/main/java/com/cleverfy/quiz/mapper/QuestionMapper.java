package com.cleverfy.quiz.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import com.cleverfy.quiz.model.Question;

public interface QuestionMapper {
	

	@SelectKey(keyProperty="id", resultType=Long.class, before=true, statement = { "SELECT NEXTVAL('cf_questions_seq') AS id" })
	@Insert("INSERT INTO CF_QUESTIONS (id, question, question_context, importance_order, quiz_id, planned_view) "
			+ "VALUES (#{id}, #{question}, #{questionContext}, #{importanceOrder}, #{quizId}, #{plannedView})")	
	public void save(Question question);
	
	@Select("SELECT q.*, a.id as answer_id, a.answer, a.correct, a.question_id  FROM CF_QUESTIONS q "
			+ "LEFT JOIN CF_ANSWERS a ON (a.question_id = q.id) "
			+ "WHERE q.id = #{questionId}")
	@ResultMap("QuestionResultMap")
	public Question findOne(Long questionId);
	
	@Select("SELECT q.*, a.id as answer_id, a.answer, a.correct, a.question_id FROM CF_QUESTIONS q "
			+ "LEFT JOIN CF_ANSWERS a ON (a.question_id = q.id) "
			+ "WHERE q.quiz_id = #{quizId} "
			+ "ORDER BY importance_order")
	@ResultMap("QuestionResultMap")
	public List<Question> findQuizQuestions(Long quizId);	

	@Delete("DELETE FROM CF_QUESTIONS q WHERE q.quiz_id IN (SELECT qi.id FROM CF_QUIZZES qi WHERE qi.id = #{quizId} AND qi.owner_user_id = #{userId})  ")
	public void deleteQuestions(@Param("quizId") Long quizId, @Param("userId") Long userId);
	
	@Select("SELECT id " +
			"FROM CF_QUESTIONS " + 
			"WHERE planned_view > current_timestamp " +
			 "AND planned_view < (SELECT MIN(planned_view) FROM CF_QUESTIONS WHERE planned_view > current_timestamp AND quiz_id IN (SELECT id FROM CF_QUIZZES qi WHERE qi.owner_user_id = #{userId} AND qi.id = #{quizId})) + interval '1 day' " +
			 "AND quiz_id IN (SELECT id FROM CF_QUIZZES qi WHERE qi.owner_user_id = #{userId} AND qi.id = #{quizId})")
	public List<Long> getNearestDayQuestions(@Param("quizId") Long quizId, @Param("userId") Long userId);
	
	@Update("<script>UPDATE CF_QUESTIONS "
			+ "SET planned_view = #{plannedView} "
			+ "WHERE id IN <foreach item=\"item\" index=\"index\" collection=\"questions\" open=\"(\" separator=\",\" close=\")\">#{item}</foreach> AND quiz_id IN (SELECT id FROM CF_QUIZZES qi WHERE qi.owner_user_id = #{userId} AND qi.id = #{quizId})</script>")
	public void updateQuestionsPlannedView(@Param("plannedView") Date plannedView, @Param("questions") List<Long> questions, @Param("quizId") Long quizId, @Param("userId") Long userId);
	
	@Select("SELECT q.id FROM CF_QUESTIONS q "
			+ "WHERE q.quiz_id = #{quizId} AND planned_view < #{viewDeadline} AND importance_order <= #{maxImportanceOrder} ORDER BY planned_view, importance_order LIMIT #{limit}")
	public List<Long> getQuestionPlan(@Param("quizId") Long quizId, @Param("viewDeadline") Date viewDeadline, @Param("maxImportanceOrder") int maxImportanceOrder, @Param("limit") long limit);
	
	@Update("UPDATE CF_QUESTIONS SET planned_view = #{plannedView} WHERE id = #{questionId}")
	public void updateQuestionPlannedView(@Param("questionId") Long questionId, @Param("plannedView") Date plannedView);
}
