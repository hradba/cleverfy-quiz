package com.cleverfy.quiz.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import com.cleverfy.quiz.model.Quiz;

public interface QuizMapper {
	

	@SelectKey(keyProperty="id", resultType=Long.class, before=true, statement = { "SELECT NEXTVAL('cf_quizzes_seq') AS id" })
	@Insert("INSERT INTO CF_QUIZZES (id, name, generation_completed, owner_user_id, user_quiz_level) VALUES (#{id}, #{name}, #{generationCompleted}, #{ownerUserId}, #{userQuizLevel})")	
	public void save(Quiz quiz);
	
	@Select("SELECT q.*, (SELECT COUNT(qe.id) FROM CF_QUESTIONS qe WHERE qe.quiz_id = q.id) as total_question_count FROM CF_QUIZZES q WHERE q.id = #{quizId}")
	@ResultMap("QuizResultMap")
	public Quiz findOne(Long quizId);
	
	@Select("SELECT q.*, (SELECT COUNT(qe.id) FROM CF_QUESTIONS qe WHERE qe.quiz_id = q.id) as total_question_count FROM CF_QUIZZES q WHERE q.owner_user_id = #{userId}")
	@ResultMap("QuizResultMap")
	public List<Quiz> findUserQuizzes(Long userId);

	@Update("UPDATE CF_QUIZZES SET generation_completed = true WHERE id = #{quizId}")
	public void markGenerationCompleted(Long quizId);
	
	@Delete("DELETE FROM CF_QUIZZES WHERE id= #{quizId} AND owner_user_id = #{userId}")
	public void deleteQuiz(@Param("quizId") Long quizId, @Param("userId") Long userId);
		
}
