package com.cleverfy.quiz.mapper;

import java.util.Date;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import com.cleverfy.quiz.model.User;

public interface UserMapper {
	

	@SelectKey(keyProperty="id", resultType=Long.class, before=true, statement = { "SELECT NEXTVAL('cf_users_seq') AS id" })
	@Insert("INSERT INTO CF_USERS "
			+ "(id, email, password, first_name, last_name, signed_up, activated, activation_hash) "
			+ "VALUES(#{id}, #{email}, #{password}, #{firstName}, #{lastName}, #{signedUp}, false, #{activationHash})")	
	public void save(User user);

	@Select("SELECT * FROM CF_USERS WHERE email = #{email}")
	@ResultMap("UserResultMap")
	public User findByEmail(String email);

	@Select("SELECT * FROM CF_USERS WHERE activation_hash = #{activationHash}")
	@ResultMap("UserResultMap")
	public User findByActivationHash(String activationHash);
	
	@Select("SELECT * FROM CF_USERS WHERE id = #{userId}")
	@ResultMap("UserResultMap")
	public User findOne(Long userId);
	
	@Update("UPDATE CF_USERS SET activated = true, signed_up = #{activationDate} WHERE id = #{userId}")
	public void setUserActivated(@Param("userId") Long userId, @Param("activationDate") Date activationDate);
}
