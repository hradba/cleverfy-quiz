package com.cleverfy.quiz.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;

import com.cleverfy.quiz.model.UserQuestionResult;

public interface UserQuestionResultMapper {
	
	@SelectKey(keyProperty="id", resultType=Long.class, before=true, statement = { "SELECT NEXTVAL('cf_users_questions_results_seq') AS id" })
	@Insert("INSERT INTO CF_USERS_QUESTIONS_RESULTS "
			+ "(id, user_id, question_id, correct, answer_submited) "
			+ "VALUES(#{id}, #{userId}, #{questionId}, #{correct}, #{answerSubmited})")	
	public void save(UserQuestionResult user);

	@Select("SELECT COUNT(*) "
			+ "FROM CF_USERS_QUESTIONS_RESULTS "
			+ "WHERE user_id = #{userId} "
				+ "AND question_id = #{questionId} "
				+ "AND answer_submited >= "
					+ "(SELECT coalesce(MAX(answer_submited), to_timestamp(0)) FROM CF_USERS_QUESTIONS_RESULTS WHERE user_id = #{userId} AND question_id = #{questionId} AND correct = false) "
			)				
	public int getInLineCorrectAnswerCount(@Param("userId") Long userId, @Param("questionId") Long questionId);
	
	@Delete("DELETE FROM CF_USERS_QUESTIONS_RESULTS WHERE user_id = #{userId} AND question_id IN (SELECT id FROM CF_QUESTIONS WHERE quiz_id = #{quizId})")
	public void deleteQuizResults(@Param("quizId") Long quizId, @Param("userId") Long userId);
}
