package com.cleverfy.quiz.model;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class Answer implements Serializable {
	
	/**
	 * 
	 */
    private static final long serialVersionUID = -2257358257858741425L;
    private Long id;
    private Long questionId;
	private String answer;	
	private boolean correct;
	
	
	public Answer() {
	    super();
    }

	public Answer(Long questionId, String answer, boolean correct) {
	    super();
	    this.questionId = questionId;
	    this.answer = answer;
	    this.correct = correct;
    }

	public Long getQuestionId() {
		return questionId;
	}

	public Long getId() {
		return id;
	}

	public String getAnswer() {
		return answer;
	}

	public boolean isCorrect() {
		return correct;
	}	
	
	
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
}
