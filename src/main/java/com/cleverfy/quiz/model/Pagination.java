package com.cleverfy.quiz.model;

import java.util.ArrayList;
import java.util.List;

public class Pagination {

	private String baseUrl;
	private int limit;
	private long total;
	private int currentPage;
	private int maxPage;	
	private List<Page> pages;
	
	
	public Pagination(String baseUrl, int offset, int limit, long total) {
	    super();
	    this.baseUrl = baseUrl;
	    this.limit = limit;
	    this.total = total;	    
	    this.maxPage = total > 0 ? (int) Math.ceil((double) total / limit) - 1 : 0;
	    
	    this.currentPage = (int) Math.ceil((double) offset / limit);
	    int firstDisplayedPage = currentPage - 2 < 0 ? 0 : currentPage - 2;
	    int lastDisplayedPage = currentPage + 2 > maxPage ? maxPage : currentPage + 2;
	    
	    
	    pages = new ArrayList<>(maxPage);
	    
	    for (int i = firstDisplayedPage; i <= lastDisplayedPage; i++) {
	    	pages.add(new Page(i * limit, "" + (i + 1), i == currentPage));
	    }
    }

	public int getFirstOffset() {
		return 0;
	}
	public int getPrevOffset() {
		return currentPage - 1 > 0 ? (currentPage - 1) * limit : 0;
	}
	public int getNextOffset() {
		return currentPage + 1 < maxPage ? (currentPage + 1) * limit : maxPage * limit;
	}
	public int getLastOffset() {
		return maxPage * limit;
	}
	
	public String getBaseUrl() {
		return baseUrl;
	}

	public int getLimit() {
		return limit;
	}

	public long getTotal() {
		return total;
	}
	
	public List<Page> getPages() {
		return pages;
	}
	
	class Page {
		private int offset;
		private String name;
		private boolean current;
		
		public Page(int offset, String name, boolean current) {
	        super();
	        this.offset = offset;
	        this.name = name;
	        this.current = current;
        }

		public int getOffset() {
			return offset;
		}

		public String getName() {
			return name;
		}

		public boolean isCurrent() {
			return current;
		}								
		
	}
}
