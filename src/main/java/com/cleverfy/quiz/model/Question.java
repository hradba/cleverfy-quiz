package com.cleverfy.quiz.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class Question implements Serializable {
	
	/**
	 * 
	 */
    private static final long serialVersionUID = 6984507681210491733L;
	private Long id;
	private Long quizId;
	private String question;
	private String questionContext;
	private Long importanceOrder;
	private List<Answer> answers;
	private Date plannedView;
	
	
	public Question() {
	    super();
	    answers = new ArrayList<>();
    }

	public Question(Long quizId, String question, String questionContext, Long importanceOrder, Date plannedView) {
	    super();
	    this.quizId = quizId;
	    this.question = question;
	    this.questionContext = questionContext;
	    this.importanceOrder = importanceOrder;
	    this.plannedView = plannedView;
	    answers = new ArrayList<>();
    }

	public Long getId() {
		return id;
	}

	public Long getQuizId() {
		return quizId;
	}

	public String getQuestion() {
		return question;
	}

	
	public String getQuestionContext() {
		return questionContext;
	}

	public List<Answer> getAnswers() {
		return answers;
	}
	
	
	
	public Date getPlannedView() {
		return plannedView;
	}

	public Long getImportanceOrder() {
		return importanceOrder;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
		
}
