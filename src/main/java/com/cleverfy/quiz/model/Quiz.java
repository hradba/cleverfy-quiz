package com.cleverfy.quiz.model;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class Quiz implements Serializable {
	
	/**
	 * 
	 */
    private static final long serialVersionUID = 279713424031325933L;
	private Long id;
	private String name;
	private boolean generationCompleted;
	private Long ownerUserId;
	private long totalQuestionCount = 0;
	private Integer userQuizLevel;
	
	public Quiz() {
	    super();
    }

	public Quiz(String name, Long ownerUserId) {
	    super();
	    this.name = name;
	    this.generationCompleted = false;
	    this.ownerUserId = ownerUserId;
	    this.userQuizLevel = 1;
    }
	
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public boolean isGenerationCompleted() {
		return generationCompleted;
	}
	

	public Long getOwnerUserId() {
		return ownerUserId;
	}
	
	
	public long getTotalQuestionCount() {
		return totalQuestionCount;
	}

	public Integer getUserQuizLevel() {
		return userQuizLevel;
	}


	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
