package com.cleverfy.quiz.model;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;

public class User implements Serializable {
	
	/**
	 * 
	 */
    private static final long serialVersionUID = -7132131519377440431L;
    private Long id;
	private String email;
	private String password;
	private String firstName;
	private String lastName;
	private Date signedUp;
	private boolean activated;
	private String activationHash;
	
	public User() {
	    super();
    }

	public User(String email, String password, String firstName, String lastName) {
	    super();
	    
	    this.email = email;
	    this.password = encodePassword(password);
	    this.firstName = firstName;
	    this.lastName = lastName;
	    this.activationHash = UUID.randomUUID().toString();
	    this.activated = false;
	    this.signedUp = new Date();
    }
	
	private String encodePassword(String plainPassword) {
		MessageDigestPasswordEncoder encoder = new MessageDigestPasswordEncoder("SHA-1");
		return encoder.encodePassword(plainPassword, "adkj3k24lj4jdaosij325489u89fas");
	}
			
	public Long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
	public boolean isActivated() {
		return activated;
	}

	public String getActivationHash() {
		return activationHash;
	}

	public boolean passwordMatches(String plainPassword) {
		if (encodePassword(plainPassword).equals(password)) {
			return true;
		} else {
			return false;
		}
	}
	
	public Date getSignedUp() {
		return signedUp;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
}
