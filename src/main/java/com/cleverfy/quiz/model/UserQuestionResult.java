package com.cleverfy.quiz.model;

import java.io.Serializable;
import java.util.Date;

public class UserQuestionResult implements Serializable {

	/**
	 * 
	 */
    private static final long serialVersionUID = 97510250767818496L;
	
    private Long id;
    private Long userId;
    private Long questionId;
    private boolean correct;
    private Date answerSubmited;
    
	public UserQuestionResult(Long userId, Long questionId, boolean correct) {
	    super();
	    this.userId = userId;
	    this.questionId = questionId;
	    this.correct = correct;
	    this.answerSubmited = new Date();
    }

	
	public Long getId() {
		return id;
	}


	public Long getUserId() {
		return userId;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public boolean isCorrect() {
		return correct;
	}

	public Date getAnswerSubmited() {
		return answerSubmited;
	}
    
    
}
