package com.cleverfy.quiz.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cleverfy.quiz.mapper.QuestionMapper;
import com.cleverfy.quiz.mapper.QuizMapper;
import com.cleverfy.quiz.mapper.UserQuestionResultMapper;
import com.cleverfy.quiz.model.Question;
import com.cleverfy.quiz.model.Quiz;
import com.cleverfy.quiz.model.UserQuestionResult;

@Service
public class QuizExecutorService {
	
	@Autowired
	private QuizService quizService;
	
	@Autowired
	private QuestionMapper questionMapper;
	
	@Autowired
	private QuizMapper quizMapper;
	
	@Autowired
	private UserQuestionResultMapper userQuestionResultMapper;
	
	private static final int[] DAYS_TO_ADD = new int[] {1, 1, 2, 4, 8, 30, 60, 120};
	private static final int QUESTIONS_PER_LEVEL = 10;
	private static final long MAXIMUM_QUESTIONS_FOR_REVIEW = 100;
	
	@Transactional
	public void addExtraQuestions(Long quizId, Long userId) {		
		List<Long> questions = questionMapper.getNearestDayQuestions(quizId, userId);
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		Date plannedView = c.getTime();
		if (questions.size() > 0) {
			questionMapper.updateQuestionsPlannedView(plannedView, questions, quizId, userId);
		}
	}
	
	public Question getCurrentQuestion(Long quizId, Date viewDeadline) {
		Quiz quiz = quizMapper.findOne(quizId);
		Integer userQuizLevel = quiz.getUserQuizLevel();
		int maxImportanceOrder = getMaxImportanceOrder(userQuizLevel);
		List<Long> questionIds = questionMapper.getQuestionPlan(quizId, viewDeadline, maxImportanceOrder, 1);
		if (questionIds.size() > 0) {
			return questionMapper.findOne(questionIds.get(0));
		} else {
			return null;
		}
	}
	
	private int getMaxImportanceOrder(Integer userQuizLevel) {
		return userQuizLevel * QUESTIONS_PER_LEVEL;
	}
	
	public long getQuestionForReviewCount(Quiz quiz) {
		List<Long> questionPlan = questionMapper.getQuestionPlan(quiz.getId(), new Date(), getMaxImportanceOrder(quiz.getUserQuizLevel()), MAXIMUM_QUESTIONS_FOR_REVIEW);
		return questionPlan.size();
		
	}
	
	public int getQuizProgress(Long quizId) {
		return 0;
	}
	
	public Boolean answer(Long quizId, Long questionId, Long userId, String answer) {
		Question currentQuestion = getCurrentQuestion(quizId, new Date());
		if (currentQuestion.getId().equals(questionId)) {						
			boolean correct = false;
			if (quizService.isCorrectAnswer(currentQuestion, answer)) {
				correct = true;
			}						
			
			UserQuestionResult userQuestionResult = new UserQuestionResult(userId, questionId, correct);
			userQuestionResultMapper.save(userQuestionResult);
			
			int correctCount = userQuestionResultMapper.getInLineCorrectAnswerCount(userId, questionId);			  
			Calendar plannedView = Calendar.getInstance();			
			plannedView.add(Calendar.DATE, correctCount < DAYS_TO_ADD.length ? DAYS_TO_ADD[correctCount] : DAYS_TO_ADD[DAYS_TO_ADD.length - 1]);
			plannedView.set(Calendar.HOUR_OF_DAY, 0);
			plannedView.set(Calendar.MINUTE, 0);
			plannedView.set(Calendar.SECOND, 0);
			plannedView.set(Calendar.MILLISECOND, 0);
			questionMapper.updateQuestionPlannedView(questionId, plannedView.getTime());
			return Boolean.valueOf(correct);
		} else {			
			return null;
		}
	}
	
}
