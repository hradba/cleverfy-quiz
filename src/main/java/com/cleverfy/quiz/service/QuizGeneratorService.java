package com.cleverfy.quiz.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Future;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;


@Service
public class QuizGeneratorService {
		
	//QuestionGenerator questionGenerator = new QuestionGenerator();
	
	@Async
	public Future<Boolean> generate(String text, GenerationListener generationListener) {
		
		/*Collection<Question> questions = questionGenerator.generate(text);
		for (Question question : questions) {
			generationListener.questionGenerated(question.getContext(), question.getQuestion(), question.getAnswer(), question.getImportanceOrder(), question.getAnswerVariants());
        }*/
		
		
		for (int i = 0; i < 100; i = i + 2) {			
			generationListener.questionGenerated("", "What is spaced repetition? (" + (i + 1) + ")", "A learning technique", Long.valueOf(i + 1), new ArrayList<String>(Arrays.asList("A sleeping technique", "Type of spaceship")));
			generationListener.questionGenerated("Spaced repetition is a learning technique", "When was spaced repetition invented? (" + (i + 2) + ")", "In 1906", Long.valueOf(i + 2), new ArrayList<String>(Arrays.asList("In 1903", "In 1907", "In 1910")));
			try {				
	            Thread.sleep(100);
            } catch (InterruptedException e) {
	            e.printStackTrace();
            }			
		}
		generationListener.quizGenerationDone();
		return new AsyncResult<Boolean>(Boolean.TRUE);
	}
	
	public interface GenerationListener {
		void questionGenerated(String context, String question, String answer, Long importanceOrder, Collection<String> wrongAnswers);
		void quizGenerationDone();
	}
}
