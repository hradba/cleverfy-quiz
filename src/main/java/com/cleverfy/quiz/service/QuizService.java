package com.cleverfy.quiz.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cleverfy.quiz.mapper.AnswerMapper;
import com.cleverfy.quiz.mapper.QuestionMapper;
import com.cleverfy.quiz.mapper.QuizMapper;
import com.cleverfy.quiz.mapper.UserQuestionResultMapper;
import com.cleverfy.quiz.model.Answer;
import com.cleverfy.quiz.model.Question;
import com.cleverfy.quiz.model.Quiz;
import com.cleverfy.quiz.service.QuizGeneratorService.GenerationListener;

@Service
public class QuizService {
	
	@Autowired
	private QuizGeneratorService quizGeneratorService;
	
	@Autowired
	private QuizMapper quizMapper;
	
	@Autowired
	private QuestionMapper questionMapper;
	
	@Autowired
	private AnswerMapper answerMapper;
	
	@Autowired
	private UserQuestionResultMapper userQuestionResultMapper;
	
	public QuizService() {
	}
	
	public Long generateQuiz(Long userId, String title, String text) {
		
		final Quiz quiz = new Quiz(title, userId);
		createQuiz(quiz);
		quizGeneratorService.generate(text, new GenerationListener() {
			@Override
			public void questionGenerated(String questionContext, String questionText, String answerText, Long importanceOrder, Collection<String> wrongAnswers) {
				Calendar todayMidnight = Calendar.getInstance();
				todayMidnight.set(Calendar.HOUR_OF_DAY, 0);
				todayMidnight.set(Calendar.MINUTE, 0);
				todayMidnight.set(Calendar.SECOND, 0);
				todayMidnight.set(Calendar.MILLISECOND, 0);
				
				Date plannedView = todayMidnight.getTime();
				Question question = new Question(quiz.getId(), questionText, questionContext, importanceOrder, plannedView);
				
				addQuizQuestion(question);
				Answer answer = new Answer(question.getId(), answerText, true);
				addQuestionAnswer(answer);
				for (String wrongAnswerText: wrongAnswers) {
					Answer wrongAnswer = new Answer(question.getId(), wrongAnswerText, false);
					addQuestionAnswer(wrongAnswer);
				}				
			}
			
			@Override
			public void quizGenerationDone() {
				markGenerationDone(quiz);
			}
		});
		
		return quiz.getId();
	}
	
	public void markGenerationDone(Quiz quiz) {
		quizMapper.markGenerationCompleted(quiz.getId());
	}
	
	public Quiz createQuiz(Quiz quiz) {
		quizMapper.save(quiz);
		return quiz;
	}
	
	public Question addQuizQuestion(Question question) {
		questionMapper.save(question);
		return question;
	}
	
	public Answer addQuestionAnswer(Answer answer) {
		answerMapper.save(answer);
		return answer;
	}
	
	public Question getQuestion(Long questionId) {
		return questionMapper.findOne(questionId);
	}
	
	public boolean isCorrectAnswer(Question question, String answerText) {
		Answer answer = answerMapper.findQuestionAnswer(question.getId());
		if (answer != null && answer.getAnswer() != null) {
			if (answer.getAnswer().equals(answerText)) return true;
		} 
		return false;
	}
	
	public List<Question> getQuizQuestions(long quizId) {
		return questionMapper.findQuizQuestions(quizId);
	}
	
	public List<Question> getQuizQuestions(long quizId, int offset, int limit) {
		List<Question> questions = getQuizQuestions(quizId);
		List<Question> filteredQuestions = new ArrayList<>();
		for (int i = offset; i < offset + limit && i < questions.size(); i++) {
			if (questions.get(i) != null) {
				filteredQuestions.add(questions.get(i));
			}
		}
		return filteredQuestions;
		
	}
	
	public List<Quiz> getUserQuizzes(Long userId) {
		return quizMapper.findUserQuizzes(userId);
	}
	
	public Quiz getUserQuiz(Long quizId, Long userId) {
		return quizMapper.findOne(quizId);
	}
	
	@Transactional
	public void deleteQuiz(Long quizId, Long userId) {
		userQuestionResultMapper.deleteQuizResults(quizId, userId);
		answerMapper.deleteAnswers(quizId, userId);
		questionMapper.deleteQuestions(quizId, userId);
		quizMapper.deleteQuiz(quizId, userId);		
	}
}