package com.cleverfy.quiz.service;

import java.util.Date;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.cleverfy.quiz.mapper.UserMapper;
import com.cleverfy.quiz.model.User;

@Service
public class UserService {

	final static Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	UserMapper userMapper;
	
	@Autowired
	JavaMailSender mailSender;

	public void signUpUser(String email, String password) {
		User user = new User(email, password, "", "");
		userMapper.save(user);
		String activationUrl = "http://www.cleverfy.com/activate/" + user.getActivationHash();
		
		String html = "Thanks for signing up for Cleverfy, your login is: "
		        + email
		        + "<br><br> Confirming your account will give you full access to the Cleverfy.<br ><br >Please visit following url: <br><br><a href=\""
		        + activationUrl + "\">" + activationUrl + "</a><br><br>Happy learning,\nCleverfy";
		String subject = "Welcome to Cleverfy, please confirm your account";
		
		try {			
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "UTF-8");
			helper.setFrom("support@cleverfy.com");
			mimeMessage.setContent(html, "text/html");
			helper.setTo(email);
			helper.setSubject(subject);
			
			mailSender.send(mimeMessage);
			logger.info("Email to " + email + " successfully sent");
		} catch (Throwable e) {			
			logger.error("Error during activation mail sending", e);
		}			
	}

	
	public void activateUser(String activationHash) {
		User user = userMapper.findByActivationHash(activationHash);
		if (user != null && !user.isActivated()) {
			userMapper.setUserActivated(user.getId(), new Date());
		}
	}
}
