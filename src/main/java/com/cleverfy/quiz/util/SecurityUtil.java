package com.cleverfy.quiz.util;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtil {
	public static Long getCurrentUserId() {		
		return (Long)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	
	public static Collection<? extends GrantedAuthority> getCurrentAuthorities() {
		return SecurityContextHolder.getContext().getAuthentication().getAuthorities();
	}
}
