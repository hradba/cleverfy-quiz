package com.cleverfy.quiz.app;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({PersistenceConfig.class, QuizApplicationConfig.class})
public class TestContext {
	
}
