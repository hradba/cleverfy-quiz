package com.cleverfy.quiz.mapper;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cleverfy.quiz.app.TestContext;
import com.cleverfy.quiz.model.Answer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class}, initializers = ConfigFileApplicationContextInitializer.class)
@Transactional
public class ITAnswerMapperTest {
	
	
	@Autowired
	AnswerMapper answerMapper;
	
	@Test
	@Rollback
	public void savedAnswerIsFindableUsinItsId() {
		Answer answer = new Answer(null, "Some answer", true);
		answerMapper.save(answer);
		Answer fAnswer = answerMapper.findOne(answer.getId());				
		assertEquals(answer, fAnswer);
	}
	

}
