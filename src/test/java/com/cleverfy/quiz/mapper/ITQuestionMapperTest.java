package com.cleverfy.quiz.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cleverfy.quiz.app.TestContext;
import com.cleverfy.quiz.model.Question;
import com.cleverfy.quiz.model.Quiz;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class}, initializers = ConfigFileApplicationContextInitializer.class)
@Transactional
public class ITQuestionMapperTest {


	@Autowired
	QuestionMapper questionMapper;
	
	@Autowired
	QuizMapper quizMapper;
	
	@Test
	@Rollback
	public void savedQuestionWithoutAnswersIsFindableUsinItsId() {
		
		Question question = new Question(null, "How are you?", "Today is nice day", 1L, new Date());
		questionMapper.save(question);
		Question fQuestion = questionMapper.findOne(question.getId());				
		assertEquals(question, fQuestion);
	}
	
	@Test
	@Rollback
	public void nearestDayQuestionsDateMustBeTheNearest() {
		Quiz quiz = new Quiz("", 1L);
		quizMapper.save(quiz);		
		Calendar tommorow = Calendar.getInstance(); 
		tommorow.setTime(new Date()); 
		tommorow.add(Calendar.DATE, 1);		
		Question question2 = new Question(quiz.getId(), "How are you 2?", "Today is nice day", 1L, tommorow.getTime());
		questionMapper.save(question2);

		Calendar dayAfterTommorow = Calendar.getInstance(); 
		dayAfterTommorow.setTime(new Date()); 
		dayAfterTommorow.add(Calendar.DATE, 2);		
		Question question3 = new Question(quiz.getId(), "How are you 3?", "Today is nice day", 1L, dayAfterTommorow.getTime());
		questionMapper.save(question3);
		
		List<Long> questions = questionMapper.getNearestDayQuestions(quiz.getId(), 1L);
		assertNotNull(questions);
		assertEquals(1, questions.size());
		assertEquals(question2.getId(), questions.get(0));
	}
	
	@Test
	@Rollback
	public void updateQuestionsPlannedViewMustUpdateOnlyGivenQuestionsToPlannedView() {
		Date today = new Date();
		Quiz quiz = new Quiz("", 1L);
		quizMapper.save(quiz);		
		Question question1 = new Question(quiz.getId(), "How are you 1?", "Today is nice day", 1L, today);
		questionMapper.save(question1);
		Calendar dayAfterTommorow = Calendar.getInstance(); 
		dayAfterTommorow.setTime(new Date()); 
		dayAfterTommorow.add(Calendar.DATE, 2);		
		Question question2 = new Question(quiz.getId(), "How are you 2?", "Today is nice day", 1L, dayAfterTommorow.getTime());
		Question question3 = new Question(quiz.getId(), "How are you 3?", "Today is nice day", 1L, dayAfterTommorow.getTime());
		questionMapper.save(question2);
		questionMapper.save(question3);
		List<Long> questions = new ArrayList<Long>(Arrays.asList(question2.getId(), question3.getId()));
		
		assertPlannedViewMustBe(question1.getId(), today);
		assertPlannedViewMustBe(question2.getId(), dayAfterTommorow.getTime());
		assertPlannedViewMustBe(question3.getId(), dayAfterTommorow.getTime());
		questionMapper.updateQuestionsPlannedView(today, questions, quiz.getId(), 1L);
		assertPlannedViewMustBe(question1.getId(), today);
		assertPlannedViewMustBe(question2.getId(), today);
		assertPlannedViewMustBe(question3.getId(), today);
		
	}
	
	private void assertPlannedViewMustBe(Long questionId, Date expected) {
		Question fQuestion = questionMapper.findOne(questionId);
		assertEquals(expected, fQuestion.getPlannedView());		
	}
	
	
}
