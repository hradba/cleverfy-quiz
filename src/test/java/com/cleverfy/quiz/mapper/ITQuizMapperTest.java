package com.cleverfy.quiz.mapper;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cleverfy.quiz.app.TestContext;
import com.cleverfy.quiz.model.Quiz;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class}, initializers = ConfigFileApplicationContextInitializer.class)
@Transactional
public class ITQuizMapperTest {
	


	@Autowired
	QuizMapper quizMapper;
	
	@Test
	@Rollback
	public void savedQuestionWithoutAnswersIsFindableUsingItsId() {		
		Quiz quiz = new Quiz("some quiz", 1L);
		quizMapper.save(quiz);
		Quiz fQuiz = quizMapper.findOne(quiz.getId());				
		assertEquals(quiz, fQuiz);
		
	}
}
