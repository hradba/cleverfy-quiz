package com.cleverfy.quiz.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cleverfy.quiz.app.TestContext;
import com.cleverfy.quiz.model.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class}, initializers = ConfigFileApplicationContextInitializer.class)
@Transactional
public class ITUserMapperTest {
	
	@Autowired
	UserMapper userMapper;
	
	@Test
	@Rollback
	public void savedUserIsFindableUsingHisId() {
		User user = new User("test@test.com", "password", "firstName", "lastName");
		userMapper.save(user);
		User fUser = userMapper.findOne(user.getId());				
		assertEquals(user, fUser);
	}

	@Test
	@Rollback
	public void savedUserIsFindableUsingHisEmail() {
		User user = new User("test@test.com", "password", "firstName", "lastName");
		userMapper.save(user);
		User fUser = userMapper.findByEmail("test@test.com");				
		assertEquals(user, fUser);
	}
	
	@Test
	@Rollback
	public void inActiveUserShouldBeActiveAfterActivation() {
		User user = new User("test@test.com", "password", "firstName", "lastName");
		userMapper.save(user);
		User fUser = userMapper.findByEmail("test@test.com");				
		assertFalse(fUser.isActivated());
		userMapper.setUserActivated(fUser.getId(), new Date());
		fUser = userMapper.findByEmail("test@test.com");
		assertFalse(user.isActivated());
	}
}
