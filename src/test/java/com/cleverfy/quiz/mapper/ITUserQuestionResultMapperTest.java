package com.cleverfy.quiz.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cleverfy.quiz.app.TestContext;
import com.cleverfy.quiz.model.Question;
import com.cleverfy.quiz.model.UserQuestionResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class}, initializers = ConfigFileApplicationContextInitializer.class)
@Transactional
public class ITUserQuestionResultMapperTest {
	
	@Autowired
	UserQuestionResultMapper userQuestionResultMapper;
	
	@Autowired
	QuestionMapper questionMapper;
	
	@Test
	@Rollback
	public void userResultMustBePersistable() {
		Question question = new Question(null, "??", "context", 1L, new Date());
		questionMapper.save(question);
		UserQuestionResult userQuestionResult = new UserQuestionResult(1L, question.getId(), true);
		userQuestionResultMapper.save(userQuestionResult);
		assertNotNull(userQuestionResult);
	}

	@Test
	@Rollback
	public void whenTwoUserSuccessResultsArePersistedThenUsersInlineCorrectAnswerCountMustBe2() {
		Question question = new Question(null, "??", "context", 1L, new Date());
		questionMapper.save(question);
		userQuestionResultMapper.save(new UserQuestionResult(1L, question.getId(), true));
		userQuestionResultMapper.save(new UserQuestionResult(1L, question.getId(), true));
		long count = userQuestionResultMapper.getInLineCorrectAnswerCount(1L, question.getId());
		assertEquals(2, count);
	}

	
	@Test
	@Rollback
	public void whenTwoUserSuccessResultsArePersistedAfterSuccessAndWrongOneThenUsersInlineCorrectAnswerCountMustBe2() {
		Question question = new Question(null, "??", "context", 1L, new Date());
		questionMapper.save(question);
		userQuestionResultMapper.save(new UserQuestionResult(1L, question.getId(), true));
		userQuestionResultMapper.save(new UserQuestionResult(1L, question.getId(), false));
		userQuestionResultMapper.save(new UserQuestionResult(1L, question.getId(), true));
		userQuestionResultMapper.save(new UserQuestionResult(1L, question.getId(), true));
		long count = userQuestionResultMapper.getInLineCorrectAnswerCount(1L, question.getId());
		assertEquals(3, count);
	}
}
