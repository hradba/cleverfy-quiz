package com.cleverfy.quiz.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cleverfy.quiz.app.TestContext;
import com.cleverfy.quiz.mapper.QuestionMapper;
import com.cleverfy.quiz.model.Answer;
import com.cleverfy.quiz.model.Question;
import com.cleverfy.quiz.model.Quiz;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class}, initializers = ConfigFileApplicationContextInitializer.class)
@Transactional
public class ITQuizExecutorService {
	
	@Autowired
	QuizService quizService;
	
	@Autowired
	QuizExecutorService quizExecutorService;
	
	@Autowired
	QuestionMapper questionMapper;
	
	@Test
	@Rollback
	public void correctAnswerToQuestionMustConfirmCorrectAnswer() {
		Quiz quiz = quizService.createQuiz(new Quiz("test", 1L));
		Question question1 = quizService.addQuizQuestion(new Question(quiz.getId(), "How much is 1 + 1?", "Math", 1L, new Date()));
		Answer question1Answer1 = quizService.addQuestionAnswer(new Answer(question1.getId(), "2", true));
		Boolean result = quizExecutorService.answer(quiz.getId(), question1.getId(), 1L, question1Answer1.getAnswer());
		assertNotNull(result);
		assertTrue(result);
	}

	@Test
	@Rollback
	public void wrongAnswerToQuestionMustConfirmWrongAnswer() {
		Quiz quiz = quizService.createQuiz(new Quiz("test", 1L));
		Question question1 = quizService.addQuizQuestion(new Question(quiz.getId(), "How much is 1 + 1?", "Math", 1L, new Date()));
		quizService.addQuestionAnswer(new Answer(question1.getId(), "2", true));
		Boolean result = quizExecutorService.answer(quiz.getId(), question1.getId(), 1L, "3");
		assertNotNull(result);
		assertFalse(result);
	}
	
	@Test
	@Rollback
	public void afterCreationQuestionShouldBeInPlanInInsertionOrder() {
		Quiz quiz = quizService.createQuiz(new Quiz("test", 1L));
		Question question1 = quizService.addQuizQuestion(new Question(quiz.getId(), "How much is 1 + 1?", "Math", 1L, new Date()));
		Answer question1Answer1 = quizService.addQuestionAnswer(new Answer(question1.getId(), "2", true));
		Question question2 = quizService.addQuizQuestion(new Question(quiz.getId(), "How much is 3 + 2?", "Math", 1L, new Date()));
		Answer question2Answer1 = quizService.addQuestionAnswer(new Answer(question2.getId(), "5", true));
		Question currentQuestion = quizExecutorService.getCurrentQuestion(quiz.getId(), new Date());
		assertEquals(question1.getId(), currentQuestion.getId());
		Boolean result = quizExecutorService.answer(quiz.getId(), question1.getId(), 1L, question1Answer1.getAnswer());
		assertNotNull(result);
		assertTrue(result);
		currentQuestion = quizExecutorService.getCurrentQuestion(quiz.getId(), new Date());
		assertEquals(question2.getId(), currentQuestion.getId());
		result = quizExecutorService.answer(quiz.getId(), question2.getId(), 1L, question2Answer1.getAnswer());
		assertNotNull(result);
		assertTrue(result);
		currentQuestion = quizExecutorService.getCurrentQuestion(quiz.getId(), new Date());
		assertNull(currentQuestion);		
	}
	
	@Test
	@Rollback
	public void firstCorrectAnsweredQuestionShouldBePlannedForTommorow() {
		Quiz quiz = quizService.createQuiz(new Quiz("test", 1L));
		Question question1 = quizService.addQuizQuestion(new Question(quiz.getId(), "How much is 1 + 1?", "Math", 1L, new Date()));
		Answer question1Answer1 = quizService.addQuestionAnswer(new Answer(question1.getId(), "2", true));
		quizExecutorService.answer(quiz.getId(), question1.getId(), 1L, question1Answer1.getAnswer());
		Question fQuestion = questionMapper.findOne(question1.getId());		
		Date tommorowMorning = tommorow();
		assertEquals(tommorowMorning, fQuestion.getPlannedView());		
	}	
	
	@Test
	@Rollback
	public void firstWrongAnsweredQuestionShouldBePlannedForTommorow() {
		Quiz quiz = quizService.createQuiz(new Quiz("test", 1L));
		Question question1 = quizService.addQuizQuestion(new Question(quiz.getId(), "How much is 1 + 1?", "Math", 1L, new Date()));
		quizService.addQuestionAnswer(new Answer(question1.getId(), "2", true));
		quizExecutorService.answer(quiz.getId(), question1.getId(), 1L, "WROOOONG");
		Question fQuestion = questionMapper.findOne(question1.getId());		
		Date tommorowMorning = tommorow();
		assertEquals(tommorowMorning, fQuestion.getPlannedView());		
	}
	
	@Test
	@Rollback
	public void secondCorrectAnsweredQuestionShouldBePlannedForDayAfterTommorow() {
		Quiz quiz = quizService.createQuiz(new Quiz("test", 1L));
		Question question1 = quizService.addQuizQuestion(new Question(quiz.getId(), "How much is 1 + 1?", "Math", 1L, new Date()));
		Answer question1Answer1 = quizService.addQuestionAnswer(new Answer(question1.getId(), "2", true));
		
		
		quizExecutorService.answer(quiz.getId(), question1.getId(), 1L, question1Answer1.getAnswer());
		
		Question fQuestion = questionMapper.findOne(question1.getId());		
		Date tommorowMorning = tommorow();
		assertEquals(tommorowMorning, fQuestion.getPlannedView());		
	}
	
	private Date tommorow() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	/*
	 		Quiz quiz = quizService.createQuiz(new Quiz("test", 1L));
		Question question1 = quizService.addQuizQuestion(new Question(quiz.getId(), "How much is 1 + 1?", "Math", 1L));
		Answer question1Answer1 = quizService.addQuestionAnswer(new Answer(question1.getId(), "2", true));
		Answer question1Answer2 = quizService.addQuestionAnswer(new Answer(question1.getId(), "3", false));
		Question question2 = quizService.addQuizQuestion(new Question(quiz.getId(), "How much is 3 + 2?", "Math", 1L));
		Answer question2Answer1 = quizService.addQuestionAnswer(new Answer(question2.getId(), "1", false));
		Answer question2Answer2 = quizService.addQuestionAnswer(new Answer(question2.getId(), "5", true));
*/
	 
}
