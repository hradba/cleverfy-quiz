package com.cleverfy.quiz.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cleverfy.quiz.app.TestContext;
import com.cleverfy.quiz.model.Answer;
import com.cleverfy.quiz.model.Question;
import com.cleverfy.quiz.model.Quiz;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class}, initializers = ConfigFileApplicationContextInitializer.class)
@Transactional
public class ITQuizServiceTest {
	
	@Autowired
	QuizService quizService;
	
	@Test
	@Rollback
	public void persistedQuizWithQuestionsAndAnswersIsFindableUsingItsId() {
		Quiz quiz = quizService.createQuiz(new Quiz("test", 1L));
		Question question1 = quizService.addQuizQuestion(new Question(quiz.getId(), "What is the answer on everything?", "Some context", 1L, new Date()));
		Question question2 = quizService.addQuizQuestion(new Question(quiz.getId(), "How much is 1+1?", "Some context", 1L, new Date()));
		
		Answer answer11 = quizService.addQuestionAnswer(new Answer(question1.getId(), "42", true));
		Answer answer12 = quizService.addQuestionAnswer(new Answer(question1.getId(), "26", false));
		Answer answer21 = quizService.addQuestionAnswer(new Answer(question2.getId(), "2", true));
		
		
		Quiz fQuiz = quizService.getUserQuiz(quiz.getId(), 1L);
		List<Question> fQuestions = quizService.getQuizQuestions(fQuiz.getId());
		assertEquals(quiz.getId(), fQuiz.getId());
		assertEquals(2, fQuiz.getTotalQuestionCount());
		
		assertEquals(2, fQuestions.get(0).getAnswers().size());
		assertEquals(answer11, fQuestions.get(0).getAnswers().get(0));
		assertEquals(answer12, fQuestions.get(0).getAnswers().get(1));
		
		
		assertEquals(1, fQuestions.get(1).getAnswers().size());
		assertEquals(answer21, fQuestions.get(1).getAnswers().get(0));
		
		
	}

	@Test
	@Rollback
	public void deletedQuizWithQuestionsAndAnswersIsNOTFindableUsingItsId() {
		Quiz quiz = quizService.createQuiz(new Quiz("test", 1L));
		Question question1 = quizService.addQuizQuestion(new Question(quiz.getId(), "What is the answer on everything?","Some context", 1L, new Date()));
		Question question2 = quizService.addQuizQuestion(new Question(quiz.getId(), "How much is 1+1?", "Some context", 1L, new Date()));
		
		Answer answer11 = quizService.addQuestionAnswer(new Answer(question1.getId(), "42", true));
		Answer answer12 = quizService.addQuestionAnswer(new Answer(question1.getId(), "26", false));
		Answer answer21 = quizService.addQuestionAnswer(new Answer(question2.getId(), "2", true));
		
		
		Quiz fQuiz = quizService.getUserQuiz(quiz.getId(), 1L);
		List<Question> fQuestions = quizService.getQuizQuestions(fQuiz.getId());
		assertEquals(quiz.getId(), fQuiz.getId());
		assertEquals(2, fQuiz.getTotalQuestionCount());
		
		assertEquals(2, fQuestions.get(0).getAnswers().size());
		assertEquals(answer11, fQuestions.get(0).getAnswers().get(0));
		assertEquals(answer12, fQuestions.get(0).getAnswers().get(1));
		
		
		assertEquals(1, fQuestions.get(1).getAnswers().size());
		assertEquals(answer21, fQuestions.get(1).getAnswers().get(0));
		
		quizService.deleteQuiz(fQuiz.getId(), 1L);
		fQuiz = quizService.getUserQuiz(quiz.getId(), 1L);
		assertNull(fQuiz);		
		
	}
	
	@Test
	@Rollback
	public void quizMarkedAsCompletedShouldBeMarkedCompletedWhenFindUsingItsId() {
		Quiz quiz = quizService.createQuiz(new Quiz("test", 1L));
		assertEquals(false, quiz.isGenerationCompleted());
		quizService.markGenerationDone(quiz);
		quiz = quizService.getUserQuiz(quiz.getId(), 1L);
		assertEquals(true, quiz.isGenerationCompleted());
		
	}
	
	@Test
	@Rollback
	public void questionWithCorrectAndIncorrectAnswerShould() {
		Question question = new Question(null, "", "", 1L, new Date());
		quizService.addQuizQuestion(question);
		Answer answer = new Answer(question.getId(), "bla 1", false);
		Answer answer2 = new Answer(question.getId(), "bla 2", true);
		quizService.addQuestionAnswer(answer);
		quizService.addQuestionAnswer(answer2);
		
		assertFalse(quizService.isCorrectAnswer(question, "bla 1"));
		assertTrue(quizService.isCorrectAnswer(question, "bla 2"));
	}
}
