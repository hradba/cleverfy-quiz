package com.cleverfy.quiz.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cleverfy.quiz.app.TestContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class}, initializers = ConfigFileApplicationContextInitializer.class)
@Transactional
public class ITUserServiceTest {

	@Autowired
	UserService userService;
	
	@Test
	@Rollback
	public void signupShouldSendEmail() {		
		//userService.signUpUser("radim.hradecky@gmail.com", "test");
	}
}
